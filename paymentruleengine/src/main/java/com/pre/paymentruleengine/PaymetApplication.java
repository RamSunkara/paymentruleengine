package com.pre.paymentruleengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymetApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymetApplication.class, args);
	}

}

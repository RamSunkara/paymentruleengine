package com.pre.paymentruleengine.dto;

public enum FundTransferType {

	IMPS, NEFT, ACCOUNT, CARD

}

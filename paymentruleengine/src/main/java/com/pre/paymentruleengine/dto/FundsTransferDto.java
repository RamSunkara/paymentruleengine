package com.pre.paymentruleengine.dto;

import java.util.Date;

public class FundsTransferDto {

	private String fromAccount;
	private String toAccount;
	private Double transactionAmount;
	private Date date;

	public String getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public String getToAccount() {
		return toAccount;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "FundsTransferDto [fromAccount=" + fromAccount + ", toAccount=" + toAccount + ", transactionAmount="
				+ transactionAmount + ", date=" + date + "]";
	}

}

package com.pre.paymentruleengine.dto;

public class TrasactionLimitDto {

	private String transactionType;
	private Double dailyMaxLimit;

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Double getDailyMaxLimit() {
		return dailyMaxLimit;
	}

	public void setDailyMaxLimit(Double dailyMaxLimit) {
		this.dailyMaxLimit = dailyMaxLimit;
	}

	@Override
	public String toString() {
		return "TrasactionLimitDto [transactionType=" + transactionType + ", dailyMaxLimit=" + dailyMaxLimit + "]";
	}

}

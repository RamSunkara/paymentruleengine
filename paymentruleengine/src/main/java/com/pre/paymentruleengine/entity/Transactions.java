package com.pre.paymentruleengine.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transactions")
public class Transactions implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4461315894482023911L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionId;
	private Long accountId;
	private Long customerId;
	private String fromAccount;
	private String toAccount;
	private Double transferedAmount;
	private String transactionType;
	private Date date;

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public String getToAccount() {
		return toAccount;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public Double getTransferedAmount() {
		return transferedAmount;
	}

	public void setTransferedAmount(Double transferedAmount) {
		this.transferedAmount = transferedAmount;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Transactions [transactionId=" + transactionId + ", accountId=" + accountId + ", customerId="
				+ customerId + ", fromAccount=" + fromAccount + ", toAccount=" + toAccount + ", transferedAmount="
				+ transferedAmount + ", transactionType=" + transactionType + ", date=" + date + "]";
	}

}

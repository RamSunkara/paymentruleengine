package com.pre.paymentruleengine.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transactionlimit")
public class TransactionLimit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5138065908108004603L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionLimitId;
	private String transactionType;
	private Double dailyMaxLimit;

	public Long getTransactionLimitId() {
		return transactionLimitId;
	}

	public void setTransactionLimitId(Long transactionLimitId) {
		this.transactionLimitId = transactionLimitId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Double getDailyMaxLimit() {
		return dailyMaxLimit;
	}

	public void setDailyMaxLimit(Double dailyMaxLimit) {
		this.dailyMaxLimit = dailyMaxLimit;
	}

	@Override
	public String toString() {
		return "TransactionLimit [transactionLimitId=" + transactionLimitId + ", transactionType=" + transactionType
				+ ", dailyMaxLimit=" + dailyMaxLimit + "]";
	}

}

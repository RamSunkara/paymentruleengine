package com.pre.paymentruleengine.exception;

public class UserAccountFundsInsuffientException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6166982030985383941L;

	UserAccountFundsInsuffientException() {
		super();
	}

	public UserAccountFundsInsuffientException(final String message) {
		super(message);
	}

}

package com.pre.paymentruleengine.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.pre.paymentruleengine.appconstants.ApplicationConstants;

@RestControllerAdvice
public class ExceptionHandlerControllerAdvice {
	@ExceptionHandler(Exception.class)
	public ExceptionResponse handleException(final Exception exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return error;
	}

	@ExceptionHandler(ToAccountDetailsNotFoundException.class)
	public ExceptionResponse handleAccounrDetailsNotFoundException(final ToAccountDetailsNotFoundException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.FAILURE_STATUS_CODE);
		return error;
	}

	@ExceptionHandler(UserAccountFundsInsuffientException.class)
	public ExceptionResponse handleInSuffientFundsException(final UserAccountFundsInsuffientException exception,
			final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(ApplicationConstants.FUNDS_INSUFFIENT);
		return error;
	}

}

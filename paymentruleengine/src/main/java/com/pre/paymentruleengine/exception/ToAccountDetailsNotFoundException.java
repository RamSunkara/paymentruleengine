package com.pre.paymentruleengine.exception;

public class ToAccountDetailsNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3410382584462006405L;

	public ToAccountDetailsNotFoundException() {
		super();
	}

	public ToAccountDetailsNotFoundException(final String message) {
		super(message);
	}

}

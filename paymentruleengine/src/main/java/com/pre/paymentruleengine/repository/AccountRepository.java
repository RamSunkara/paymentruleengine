package com.pre.paymentruleengine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pre.paymentruleengine.entity.Account;
import java.lang.String;
import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findByAccountNumber(String accountnumber);

}

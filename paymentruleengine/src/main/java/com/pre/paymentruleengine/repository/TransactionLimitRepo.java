package com.pre.paymentruleengine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pre.paymentruleengine.entity.TransactionLimit;

public interface TransactionLimitRepo extends JpaRepository<TransactionLimit, Long> {

	List<TransactionLimit> findAll();

}

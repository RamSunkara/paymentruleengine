package com.pre.paymentruleengine.appconstants;

public class ApplicationConstants {

	private ApplicationConstants() {

	}

	public static final Integer FAILURE_STATUS_CODE = 904;
	public static final String FAILURE_STATUS_MESSAGE = "We didnt found ToAccount Details,Please enter correct account details ";

	public static final Integer FUNDS_INSUFFIENT = 906;
	public static final String FUNDS_INSUFFIENT_MESSAGE = "In your account amount is insuffient,please credit amount and do the transfer ";
}

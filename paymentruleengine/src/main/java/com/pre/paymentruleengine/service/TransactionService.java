package com.pre.paymentruleengine.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pre.paymentruleengine.dto.FundsTransferDto;
import com.pre.paymentruleengine.dto.ResponseDto;
import com.pre.paymentruleengine.dto.TrasactionLimitDto;

@Service
public interface TransactionService {

	public List<TrasactionLimitDto> getDailylimit();

	public ResponseEntity<ResponseDto> userFundsTransfer(FundsTransferDto fundsTransferDto);
	

}

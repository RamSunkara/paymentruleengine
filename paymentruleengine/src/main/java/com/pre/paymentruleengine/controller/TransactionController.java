package com.pre.paymentruleengine.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pre.paymentruleengine.dto.FundsTransferDto;
import com.pre.paymentruleengine.dto.ResponseDto;
import com.pre.paymentruleengine.dto.TrasactionLimitDto;
import com.pre.paymentruleengine.service.TransactionService;

@RestController
public class TransactionController {

	@Autowired
	TransactionService transactionService;

	Logger logger = LoggerFactory.getLogger(TransactionController.class);

	@PostMapping("/funds/transactions")
	public ResponseEntity<ResponseDto> fundsTransfer(@RequestBody FundsTransferDto fundsTransferDto) {

		return transactionService.userFundsTransfer(fundsTransferDto);

	}

	/**
	 * @return List<TrasactionLimitDto>
	 * 
	 * @param transactionType :String
	 * 
	 * @param dailyMaxLimit   :Double
	 * 
	 *                        Here we are returning the list of daily max limits per
	 *                        day like NEFT and IMPS and CARD and IM..etc
	 * 
	 */
	@GetMapping("/transactions/limits")
	public List<TrasactionLimitDto> getTransactionlimits() {
		logger.info("Getting the daily max limit");
		return transactionService.getDailylimit();

	}

}

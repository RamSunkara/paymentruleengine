package com.pre.paymentruleengine.serviceimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pre.paymentruleengine.appconstants.ApplicationConstants;
import com.pre.paymentruleengine.controller.TransactionController;
import com.pre.paymentruleengine.dto.FundsTransferDto;
import com.pre.paymentruleengine.dto.ResponseDto;
import com.pre.paymentruleengine.dto.TrasactionLimitDto;
import com.pre.paymentruleengine.entity.Account;
import com.pre.paymentruleengine.entity.TransactionLimit;
import com.pre.paymentruleengine.entity.Transactions;
import com.pre.paymentruleengine.exception.ToAccountDetailsNotFoundException;
import com.pre.paymentruleengine.exception.UserAccountFundsInsuffientException;
import com.pre.paymentruleengine.repository.AccountRepository;
import com.pre.paymentruleengine.repository.TransactionLimitRepo;
import com.pre.paymentruleengine.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionLimitRepo transactionLimitRepo;

	@Autowired
	AccountRepository accountRepo;

	ModelMapper modelMapper = new ModelMapper();

	Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

	@Override
	public List<TrasactionLimitDto> getDailylimit() {
		List<TransactionLimit> dailyLimitInfo = transactionLimitRepo.findAll();

		List<TrasactionLimitDto> listOfDailyLimitDto = new ArrayList<TrasactionLimitDto>();

		logger.info("Here we are getting the list of transaction limits in transaction service layer");

		for (TransactionLimit limit : dailyLimitInfo) {

			TrasactionLimitDto transactionInfoDto = modelMapper.map(limit, TrasactionLimitDto.class);

			listOfDailyLimitDto.add(transactionInfoDto);

		}

		return listOfDailyLimitDto;
	}

	@Override
	public ResponseEntity<ResponseDto> userFundsTransfer(FundsTransferDto fundsTransferDto) {

		Account fromAccountDetails = accountRepo.findByAccountNumber(fundsTransferDto.getFromAccount());

		Account toAccountDetails = accountRepo.findByAccountNumber(fundsTransferDto.getToAccount());

		if (toAccountDetails == null) {
			throw new ToAccountDetailsNotFoundException(ApplicationConstants.FAILURE_STATUS_MESSAGE);
		}

		if (fundsTransferDto.getTransactionAmount() < fromAccountDetails.getAmount()) {

			throw new UserAccountFundsInsuffientException(ApplicationConstants.FUNDS_INSUFFIENT_MESSAGE);

		}

		Transactions transactions = new Transactions();
		
		transactions.setAccountId(fromAccountDetails.getAccountId());
		transactions.setCustomerId(fromAccountDetails.getCustomerId());
	//	transactions.setDate(fundsTransferDto.getDate());
		
		

		return null;
	}

}
